
FROM golang

RUN mkdir -p /go/src/example/
COPY ./ /go/src/example

WORKDIR /go/src/example
EXPOSE 8082

ENTRYPOINT ["sh", "sh/entrypoint.sh"]
