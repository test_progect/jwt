package main

import (
	"example/api"
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("start")

	// http://localhost:8081/get?user=ivan
	http.HandleFunc("/get", api.GenerateJWT)
	// http://localhost:8081/check?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MTg2OTc3MTQsInVzZXJOYW1lIjoiaXZhbiJ9.8g1qm17b5mc6vCTwxN5InIKmisdFXHeFCYvZYRwvhIU
	http.HandleFunc("/check", api.CheckJWT)

	err := http.ListenAndServe(":80", nil)
	if err != nil {
		fmt.Println("err: ", err)
	}
	fmt.Println("end")
}
