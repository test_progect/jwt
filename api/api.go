package api

import (
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
)

const tokenJWT string = "xT]}D$un3F5TA2T8mvn0hmtKr6wt8eKMtnp9]Um}0yg!dNtphloP7e2B9uN-}uM4G"

var jwtRefreshED time.Duration

func GenerateJWT(w http.ResponseWriter, r *http.Request) {
	user, _ := r.URL.Query()["user"]
	jwtRefreshED = 999999
	token, _ := generateJwtTocken(user[0], jwtRefreshED)
	fmt.Fprint(w, user[0]+":"+token)
}

func CheckJWT(w http.ResponseWriter, r *http.Request) {
	token, _ := r.URL.Query()["token"]
	jwtRefreshED = 999999
	tokenData, _ := extractDataJwtTocken(token[0])
	user := string(tokenData["userName"].(string))
	fmt.Fprint(w, "user: "+user)
}

// ---------------------------- help -------------------------------------- //
// GenerateJwtTocken Generate JWT token
func generateJwtTocken(userName string, lifeTime time.Duration) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	// Create a parameter storage map
	claims := token.Claims.(jwt.MapClaims)

	// Create verification parameters
	claims["userName"] = userName
	claims["exp"] = time.Now().Add(time.Minute * lifeTime).Unix()

	// Sign the token with a secret key
	tokenString, err := token.SignedString([]byte(tokenJWT))
	return tokenString, err
}

// ExtractDataJwtTocken extract data from JWT tocken
func extractDataJwtTocken(tokenStr string) (jwt.MapClaims, error) {
	kfunc := func(token *jwt.Token) (interface{}, error) {
		return []byte(tokenJWT), nil
	}

	token, err := jwt.Parse(tokenStr, kfunc)
	if err != nil {
		return nil, errors.Wrap(err, "jwt parse error")
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims, nil
	}

	// PLEASE, DONT THROW BOOL FROM METHOD! WTF!? WHEN CAN ERROR OBJ!
	return nil, fmt.Errorf("Invalid JWT Token")
}
